__author__ = 'Jacek'
# -*- coding: utf-8 -*-

import json
from MapReduce import MapReduce
from pprint import pprint


def map(item):
    yield item[1], [item[0], item[2], item[3], item[4], item[5], item[6], item[7], item[8], item[9]]

def reduce(key, values):
    return (key, values)


if __name__ == '__main__':
    dane = []
    with open('records.json') as f:
        for line in f:
            dane.append(json.loads(line))

    mapper = MapReduce(map, reduce)
    results = mapper(dane)

    for a in results:
        print str(a[0]) + str(a[1])
        #for a in input_data:
        #   print a