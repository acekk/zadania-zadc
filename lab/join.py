# -*- coding: utf-8 -*-

import json
from MapReduce import MapReduce
from pprint import pprint


def map(item):
    key=int(item[1])
    del item[1]
    yield key, item


def reduce(key, values):
    wynik=[]
    for line in values[1:]:
        wynik.append([str(key)]+values[0]+line)
    return wynik


if __name__ == '__main__':
    data = []
    with open('records.json') as f:
        for line in f:
            data.append(json.loads(line))
    mapper = MapReduce(map, reduce)
    results = mapper(data)
    for row in results:
        for line in row:
            print line